'use strict';
var path = require('path');
var AdmZip = require('adm-zip');
var Promise = require("bluebird");
var fs = Promise.promisifyAll(require('fs'));

module.exports = (function () {
    var aTimer;
    var aFtpClient;

    var processListing = function (directoryItems) {
        console.log('foreach');
        var itemsToDownload = [];
        directoryItems.forEach(function (element, index, array) {
            //Ignore directories
            if (element.type === 'd') {
                console.log('directory ' + element.name);
                return;
            }
            //Ignore non zips
            if (path.extname(element.name) !== '.zip') {
                console.log('ignoring ' + element.name);
                return;
            }
            //Download zip
            itemsToDownload.push({
                source: element.name,
                destination: element.name
            });
            //aftpSystem.downloadFile(element.name, element.name);
        });
        console.log('after foreach');
        return itemsToDownload;
    };


    var processItem = function (object) {
        return aFtpClient.getAsync(object.source);
    };

    var downloadItem = function (object) {
        //console.dir(object);
        object[0].result.once('close', function () {
            //c.end();
            console.log('closed');
        });
        object[0].result.pipe(fs.createWriteStream(process.cwd() + "/zips/" + object[0].input.destination));
        console.log('downloaded: ' + object[0].input.destination);
    };

    var downloadFiles = function () {
        console.log('downloading files');
        aFtpClient.
        listAsync().
        then(processListing).
        map(function (object) {
            return processItem(object).then(function (processResult) {
                return {
                    input: object,
                    result: processResult
                };
            });
        }).
        then(downloadItem).
        then(getFiles).
        map(function (filename) {
            return unzipFile(filename)
        }).
        then(function () {
            console.log('sdf');
        }).
        catch (TypeError, function (e) {
            console.dir(e);
        }).
        catch (Promise.RejectionError, function (e) {
            console.error("unable to read file, because: ", e.message);
        });
        console.log('after downloading files');
    };

    var getFiles = function () {
        console.log('got files');
        return fs.readdirAsync(process.cwd() + "/zips");
    };

    var getXMLContent = function (filename) {
        console.log('got file: '+filename);
        return fs.readFileAsync(process.cwd() + "/zips/" + filename, "utf8");
    };

    var unzipFile = function (filename) {
        console.log('unzipping: '+process.cwd() + "/zips/" + filename);
        var zip = new AdmZip(process.cwd() + "/zips/" + filename);
        zip.extractAllTo(process.cwd() + "/zips/extracted", /*overwrite*/ true);
    };

    var processFiles = function () {
        var files = fs.readdirSync(process.cwd() + "/zips");
        console.dir(files);
        files.forEach(function (file, index, array) {
            var zip = new AdmZip(process.cwd() + "/zips/" + file);
            zip.extractAllTo(process.cwd() + "/zips/extracted", /*overwrite*/ true);
        });
    };

    var elapsed = function () {
        console.log('elapsed');
        downloadFiles();
        //Currently not correct place to process files as they could still be downloading
        //processFiles();
        //Currently in wrong place to start
        aTimer.start();
    };

    var mmsProcessor = function (timer, ftpClient) {
        aTimer = timer(elapsed);
        aTimer.start();

        aFtpClient = Promise.promisifyAll(ftpClient);
    };

    return mmsProcessor;
}());