'use strict';
var fs = require('fs');
var Promise = require("bluebird");
module.exports = function (ftpClient) {
    var aFtpClient = Promise.promisifyAll(ftpClient);

    var getListing = function (callback) {
        aFtpClient.list(callback);
    };

    var downloadFiles = function (itemsToDownload) {
        itemsToDownload.forEach(function (item) {
            aFtpClient.getAsync(item.source).then(function (stream) {
                stream.once('close', function () {
                    //c.end();
                });
                stream.pipe(fs.createWriteStream(item.destination));
                console.log('downloaded: ' + item.source);
            });

        }).then(function () {
            return Promise.promisifyAll({
                works: "doubtit"
            });
        });

    };

    return {
        getListing: getListing,
        downloadFiles: downloadFiles
    };
};