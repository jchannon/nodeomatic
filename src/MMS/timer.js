'use strict';
module.exports = function (elapsed) {
    var start = function () {
        setTimeout(function () {
            elapsed();
        }, 1000);
    };

    return {
        start: start
    };

};